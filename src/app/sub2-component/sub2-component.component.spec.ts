import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Sub2ComponentComponent } from './sub2-component.component';

describe('Sub2ComponentComponent', () => {
  let component: Sub2ComponentComponent;
  let fixture: ComponentFixture<Sub2ComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Sub2ComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Sub2ComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
